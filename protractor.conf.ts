import {Config} from 'protractor';

export let config: Config = {
  framework: 'jasmine',

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
  },

  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // Spec patterns are relative to the configuration file location passed to protractor.
  specs: ['./**/*.spec.js'],

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome'
  },

  suites: {
    "test-01": 'e2e/google1.spec.js',
    "test-02": 'e2e/google2.spec.js'
  },

  params: {
    longWait: 5000,
    shortWait: 1000
  },

  // multiCapabilities: [{
  //   browserName: 'chrome'
  // }, {
  //   browserName: 'firefox'
  // }]
};
