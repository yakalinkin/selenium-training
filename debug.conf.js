"use strict";
exports.config = {
  framework: 'jasmine',
  capabilities: {
    browserName: 'chrome'
  },
  seleniumAddress: 'http://localhost:4444/wd/hub',

  specs: ['./**/*.spec.ts'],

  suites: {
    "test-01": 'e2e/google1.spec.ts',
    "test-02": 'e2e/google2.spec.ts'
  },

  params: {
    longWait: 3000,
    shortWait: 1000
  },

  beforeLaunch: function() {
    require('ts-node').register({
      project: '.'
    });
  }
};
