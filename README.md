# Selenium training

#### Setup

```bash
yarn install
```

```bash
webdriver-manager update
```

```bash
webdriver-manager start
```

#### Run the test

```bash
yarn test --suite <your-test>
```

###### Example

```bash
yarn test --suite test-01
```
