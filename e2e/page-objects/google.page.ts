import { browser, element, by, $, $$ } from 'protractor';

export default class GooglePage {
  inputQ = element.all(by.name('q')).first();
  URIList = $$('.r > a');
  currentPage = $('#navcnt .cur');
  buttonSearch = {
    K: element(by.name('btnK')),
    G: element(by.name('btnG'))
  };

  search = async (query, key) => {
    await this.inputQ.sendKeys(query);
    await this.buttonSearch[key].click();
  };

  getURI = {
    /**
     * Get all elements.
     */
    all: () => {
      return this.URIList.map((elm) => elm.getAttribute('href'));
    },

    /**
     * Get all elements that do not match the given `text`.
     * @param {string} text
     */
    not: (text: string) => {
      const regex = new RegExp(text, 'g');

      return this.URIList
        .filter((elm) => elm.getAttribute('href').then((uri) => {
          const matchesArray = uri.match(regex);
          return !matchesArray;
        }))
        .map((elm) => elm.getAttribute('href'));
    },

    /**
     * Get all elements on the page `num`.
     * @param {number} num
     */
    onPage: (num: number) => {
      const nextPage = $(`[aria-label="Page ${num}"]`);

      nextPage.click();

      return this.URIList.map((elm) => elm.getAttribute('href'));
    }
  };

  /**
   * Add 20 results per page
   */
  showResults20PerPage = async () => {
    // Open "Settings"
    await $$('[href="/preferences"]').first().click();

    // Open "Search Settings"
    await $$('._VJq').first().click();

    browser.driver.sleep(5000);

    const source = $(`.goog-slider-thumb`);
    const target = { x: 38, y: 0 };
    const buttonSave = $('#form-buttons > .jfk-button-action');

    // Set the number of results per page
    await browser.actions().dragAndDrop(source, target).perform();

    // And save
    await buttonSave.click();
    await browser.switchTo().alert().accept();
  };
};
