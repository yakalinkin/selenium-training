//
// Protractor for Non-Angular application
//

import { browser, element, by } from 'protractor';

import { GooglePage } from './page-objects';
import { logUriList } from './utils';

browser.waitForAngularEnabled(false);

describe('Test 01: Google', () => {
  const google = new GooglePage();

  beforeAll(() => {
    browser.get('http://google.com/');
  });

  it('should search by `protractor`', async () => {
    await google.search('protractor', 'K');

    expect(google.inputQ.getAttribute('value')).toEqual('protractor');
    expect(google.URIList.count()).not.toBe(undefined);
  });

  it('should search by `protractor` on Page 3', async () => {
    await google.getURI.onPage(3);

    expect(google.inputQ.getAttribute('value')).toEqual('protractor');
    expect(google.URIList.count()).not.toBe(undefined);
    expect(google.currentPage.getText()).toEqual('3');
  });

  it('should search by `jasmine` on Page 2', async () => {
    await google.inputQ.clear();
    await google.search('jasmine', 'G');
    await google.getURI.onPage(2);

    expect(google.inputQ.getAttribute('value')).toEqual('jasmine');
    expect(google.URIList.count()).not.toBe(undefined);
    expect(google.currentPage.getText()).toEqual('2');
  });
});
