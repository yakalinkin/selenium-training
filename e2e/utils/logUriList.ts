export default function logUriList(uriList, log) {
  console.log(`\n${log}`);
  console.log(`Found ${uriList.length} results`)
  console.log('---');

  uriList.forEach((uri) => {
    console.log(uri);
  });
}
