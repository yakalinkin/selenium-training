//
// Protractor for Non-Angular application
//

import { browser, element, by } from 'protractor';

import { GooglePage } from './page-objects';
import { logUriList } from './utils';

browser.waitForAngularEnabled(false);

describe('Test 02: Google', () => {
  const google = new GooglePage();

  beforeAll(() => {
    browser.get('http://google.com/');
  });

  it('should show more than 10 results per page', async () => {
    await google.search('sciencesoft', 'K');
    await google.showResults20PerPage();

    expect(google.URIList.count()).not.toEqual(0);
    expect(google.URIList.count()).not.toEqual(10);
    expect(google.URIList.count()).toEqual(20);
    expect(google.URIList.count()).not.toBeGreaterThan(30);
  });
});
